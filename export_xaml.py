# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
	"name": "XAML Viewport3D format (.xaml)",
	"author": "Henry de Jongh (https://00laboratories.com/)",
	"version": (0, 3),
	"blender": (2, 78, 0),
	"location": "File > Export > XAML Viewport3D (.xaml) ",
	"description": "Export XAML Viewport3D",
	"warning": "",
	"category": "Import-Export",
}

import bpy
from bpy.props import StringProperty, BoolProperty
from bpy_extras.io_utils import ExportHelper
import os

class XamlExporter(bpy.types.Operator, ExportHelper):
	"""Save XAML 3D mesh data"""
	bl_idname = "export_mesh.xaml"
	bl_label = "XAML Viewport3D"

	filename_ext = ".xaml"
	filter_glob = StringProperty(default="*.xaml", options={'HIDDEN'})
	
	def execute(self, context):
	
		scene = bpy.context.scene
		
		# begin writing to the output file.
		file = open(self.filepath, 'w', encoding='utf-8')
		fw = file.write
	
		# iterate through each object of type 'MESH' in the scene:
		for obj in [ob for ob in scene.objects if ob.type=="MESH"]:
			# create a mesh that has all modifiers applied.
			mesh = obj.to_mesh(scene, True, 'PREVIEW')
			# recalculate tessellation faces.
			mesh.update(calc_tessface=True)
			
			# store data about the mesh geometry in arrays.
			vertices  = []
			normals   = []
			uvs       = []
			imagename = ''
				
			# try finding the texture name.
			active_uv_layer = mesh.tessface_uv_textures.active
			if active_uv_layer:
				active_uv_layer = active_uv_layer.data
				if obj.active_material:
					if obj.active_material.active_texture:
						imagename = obj.active_material.active_texture.name
			
			# iterate through the mesh's tessellated polygons:
			for poly in mesh.tessfaces:
				i0 = poly.vertices[0]
				i1 = poly.vertices[1]
				i2 = poly.vertices[2]
				smooth = poly.use_smooth
				vertices.append(mesh.vertices[i0].co)
				vertices.append(mesh.vertices[i1].co)
				vertices.append(mesh.vertices[i2].co)
				if smooth:
					normals.append(mesh.vertices[i0].normal)
					normals.append(mesh.vertices[i1].normal)
					normals.append(mesh.vertices[i2].normal)
				else:
					normals.append(poly.normal)
					normals.append(poly.normal)
					normals.append(poly.normal)
				if active_uv_layer:
					uvs.append(active_uv_layer[poly.index].uv1[0])
					uvs.append(1.0 - active_uv_layer[poly.index].uv1[1])
					uvs.append(active_uv_layer[poly.index].uv2[0])
					uvs.append(1.0 - active_uv_layer[poly.index].uv2[1])
					uvs.append(active_uv_layer[poly.index].uv3[0])
					uvs.append(1.0 - active_uv_layer[poly.index].uv3[1])
				if len(poly.vertices) == 4:
					i3 = poly.vertices[3]
					vertices.append(mesh.vertices[i2].co)
					vertices.append(mesh.vertices[i3].co)
					vertices.append(mesh.vertices[i0].co)
					if smooth:
						normals.append(mesh.vertices[i2].normal)
						normals.append(mesh.vertices[i3].normal)
						normals.append(mesh.vertices[i0].normal)
					else:
						normals.append(poly.normal)
						normals.append(poly.normal)
						normals.append(poly.normal)
					if active_uv_layer:
						uvs.append(active_uv_layer[poly.index].uv3[0])
						uvs.append(1.0 - active_uv_layer[poly.index].uv3[1])
						uvs.append(active_uv_layer[poly.index].uv4[0])
						uvs.append(1.0 - active_uv_layer[poly.index].uv4[1])
						uvs.append(active_uv_layer[poly.index].uv1[0])
						uvs.append(1.0 - active_uv_layer[poly.index].uv1[1])

			# <GeometryModel3D>
			fw('<GeometryModel3D>\n')
			
			#	<GeometryModel3D.Geometry>
			fw('\t<GeometryModel3D.Geometry>\n')
			
			#		<MeshGeometry3D Positions="" TriangleIndices="" Normals=""/>
			fwPositions = ''
			for vertex in vertices:
				v = obj.matrix_world * vertex
				fwPositions += "%.6f " % v[0]
				fwPositions += "%.6f " % v[1]
				fwPositions += "%.6f " % v[2]
			fwNormals = ''
			for normal in normals:
				fwNormals += "%.6f " % normal[0]
				fwNormals += "%.6f " % normal[1]
				fwNormals += "%.6f " % normal[2]
			fwTextureCoordinates = ''
			for uv in uvs:
				fwTextureCoordinates += "%.6f " % uv
			fw('\t\t<MeshGeometry3D Positions="' + fwPositions + '" Normals="' + fwNormals + '" TextureCoordinates="' + fwTextureCoordinates + '" />\n')
			
			#	</GeometryModel3D.Geometry>
			fw('\t</GeometryModel3D.Geometry>\n')
			
			#	<GeometryModel3D.Material>
			fw('\t<GeometryModel3D.Material>\n')
			
			if imagename != '':
				#		<DiffuseMaterial>
				fw('\t\t<DiffuseMaterial>\n')
				
				#			<DiffuseMaterial.Brush>
				fw('\t\t\t<DiffuseMaterial.Brush>\n')
				
				#				<ImageBrush ImageSource="pack://application:,,,/Resources/" />
				fw('\t\t\t\t<ImageBrush ImageSource="pack://application:,,,/Resources/' + imagename + '" ViewportUnits="Absolute" TileMode="Tile" />\n')
				
				#			</DiffuseMaterial.Brush>
				fw('\t\t\t</DiffuseMaterial.Brush>\n')
				
				#		</DiffuseMaterial>
				fw('\t\t</DiffuseMaterial>\n')
			else:
				#		<DiffuseMaterial Brush="#"/>
				color = 'cccccc'
				if obj.active_material:
					color  = '{0:02x}'.format(int(obj.active_material.diffuse_color[0] * 255))
					color += '{0:02x}'.format(int(obj.active_material.diffuse_color[1] * 255))
					color += '{0:02x}'.format(int(obj.active_material.diffuse_color[2] * 255))
				fw('\t\t<DiffuseMaterial Brush="#' + color + '"/>\n')
			
			#	</GeometryModel3D.Material>
			fw('\t</GeometryModel3D.Material>\n')
			
			# </GeometryModel3D>
			fw('</GeometryModel3D>\n')
			
		# finished writing to the output file.
		file.close()
		
		return {'FINISHED'}

def menu_export(self, context):
	self.layout.operator(XamlExporter.bl_idname, text="XAML Viewport3D (.xaml)")

def register():
	bpy.utils.register_module(__name__)
	bpy.types.INFO_MT_file_export.append(menu_export)


def unregister():
	bpy.utils.unregister_module(__name__)
	bpy.types.INFO_MT_file_export.remove(menu_export)
	
if __name__ == "__main__":
	register()