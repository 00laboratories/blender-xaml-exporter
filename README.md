# README #

This is a Blender Add-on to export XAML code for use with <Viewport3D/> in WPF.

![Scheme](export_xaml.png)

# Features #
* Currently generates a <GeometryModel3D/> for every mesh object in the scene.
* Diffuse colors.
* Textures with automatic "pack://application:" statements.
* Smooth / Flat shading.

# Contributing #
Please feel free to send pull requests or talk with us in the Issue Tracker!